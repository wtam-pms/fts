<?php

require_once "common.php";

head($C_TITLES[2]);

?>
    <section>
        <header>
            <button class="new-activity btn-green"><i>+</i> New Activity</button>
            <div class="activity-filter-wrapper input-wrapper">
                <i class="fa fa-search input-icon" aria-hidden="true"></i>
                <input class="activity-filter" type="text">
            </div>
        </header>

        <ul class="activity-list"></ul>

    </section>
    <div class="helper-popups">
        <input id="sp-control" type='text'/>
        <ul id='activity-context-menu'>
            <li><div>Rename</div></li>
            <li><div>Change color</div></li>
        </ul>
        <div id="new-activity-modal" class="dim-click-hide fts_modal">
            <label for="new-activity-name">Activity name:</label>
            <div class="input-wrapper">
                <i class="fa fa-pencil input-icon" aria-hidden="true"></i>
                <input id="new-activity-name" type="text" value="">
            </div>
            <button class="btn-green">Create</button>
        </div>
        <div id="activity-rename-modal" class="dim-click-hide fts_modal">
            <label for="activity-rename-input">New name for activity:</label>
            <div class="input-wrapper">
                <i class="fa fa-pencil input-icon" aria-hidden="true"></i>
                <input id="activity-rename-input" type="text" value="">
            </div>
            <button class="btn-green">Rename</button>
        </div>
    </div>
<?php

footer();