<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>About | FTS</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/about.css">
</head>
<body>
    <header class="clear-after" >
        <nav>
            <a href="#"><abbr title="For Track's Sake">FTS</abbr></a><a href="#inception">Inception</a><a href="#product">Product</a><a href="#appendix">Appendix</a>
        </nav>
        <a href="index.php">Sign in</a>
    </header>
    <article>
        <header>
            <div class="article-heading">
                <h1>For Track's Sake</h1>
                <p>A smart solution for time tracking</p>
            </div>
            <blockquote>
                <p>“Those who make the worst use of their time are the first to complain of its brevity.”</p>
                    <cite><a href="https://en.wikipedia.org/wiki/Jean_de_La_Bruy%C3%A8re">— Jean de La Bruyère, Les Caractères</a></cite>
            </blockquote>
        </header>

        <section>
            <span id="inception"></span>
            <div class="section-content">
                <h2>Inception</h2>
                <p>
                    <em>For Track's Sake</em> (<abbr title="For Track's Sake">FTS</abbr>) as an idea was born at the beginning of this school
                    year in the H3 classroom. The idea was discussed by three people - <em>Peter Paulovics</em>,
                    <em>Stanislav Krajčovič</em> and <em>Martin Maco</em>. All of us has poured their design ideas, personalities
                    and usability opinions into the mix. Of course, it would not have been possible without the help and
                    advices of our tutors <em><a href="https://dai.fmph.uniba.sk/w/Jan_Kluka/en">Ján Kľuka</a></em>
                    and <em><a href="http://davinci.fmph.uniba.sk/~darjanin1/">Milan Darjanin</a></em>.
                </p>
            </div>
        </section>

        <section>
            <span id="product"></span>
            <div class="section-content">
                <h2>Product</h2>
                <p>
                    <abbr title="For Track's Sake">FTS</abbr> is a web application which allows users to track their time and thus <em>greatly improve their time management skills</em>. The website is divided into three separate subpages (you can read about them in the
                    <a href="#section-features">Features section</a> below. Each section covers a different aspect of our product.
                </p>

                <section id="section-features">
                    <h3>Features</h3>
                    <dl>
                        <dt>Tracking of activities</dt>
                        <dd>
                            <div class="description">
                                <p>
                                    The main purpose of our application is to allow users to track their activities. They can do so by either adding activity records <strong>manually</strong> or using our <strong>live tracking system</strong>.
                                </p>
                                <p>
                                    Activity records are fully editable <em>at any time</em>, allowing users to reassign any record's activity or adjust it's time span.
                                </p>
                            </div>
                            <figure>
                                <a href="images/screen_1.png" title="Tracking subpage"><img alt="Tracking subpage" src="images/screen_1.png"></a>
                                <figcaption>The "Tracking" subpage with active live tracking</figcaption>
                            </figure>
                        </dd>
                        <dt>Reviewing reports</dt>
                        <dd>
                            <div class="description">
                                <p>
                                    Our customers can easily review computed statistics of their recorded data of <strong>any day, week, month</strong> or <strong>any other custom period of time</strong>. Summarized activity data are displayed in a comprehensive graphical manner.
                                </p>
                                <p>
                                    The way periodic reports are designed allows users to <em>extract useful information</em> from their recorded data. Some examples could be detecting immense amount of time spent on bad habits, or lack of sleep.
                                </p>
                                <p>
                                    In the <em>future versions of our application</em> we also plan to add options to extract a report to a spreadsheet or a pdf file. Not with this upcoming update, nor with the current version of the application is it possible for other users to access your data - we ensure you <strong>complete safety</strong>!
                                </p>
                            </div>
                            <figure>
                                <a href="images/screen_2.png" title="Reports subpage"><img alt="Reports subpage" src="images/screen_2.png"></a>
                                <figcaption>Overview of the "Reports" subpage with example graphs</figcaption>
                            </figure>
                        </dd>
                        <dt>Activity management</dt>
                        <dd>
                            <div class="description">
                                <p>
                                    Activity records with their assigned activities can be <em>easily</em> managed, allowing the user to <strong>rename them</strong>, <strong>change their colour</strong> or <strong>delete them</strong>. Changing an activity also alters <em>all records</em> associated with it, allowing <strong>mass activity record editing</strong>.
                                </p>
                            </div>
                            <figure>
                                <a href="images/screen_3.png" title="Activities subpage"><img alt="Activities subpage" src="images/screen_3.png"></a>
                                <figcaption>The "Activities" subpage showing the list of predefined activities</figcaption>
                            </figure>
                        </dd>
                        <dt>Smart assistance</dt>
                        <dd>
                            <div class="description">
                                <p>
                                    Every action the user has to take, beginning with account registration, through activity management up to recording activities is designed to consist of <strong>the smallest possible amount of steps</strong>.
                                </p>
                                <p>Users are constantly being assisted by the application via searching mechanisms and auto-completion. There's also no need to worry about saving your data - this action is done automatically <em>every time user makes a change</em>.
                                </p>
                            </div>
                            <figure>
                                <a href="images/screen_4.png" title="Dropdown activity suggestion"><img alt="Dropdown activity suggestion" src="images/screen_4.png"></a>
                                <figcaption>An efficient way of choosing activities</figcaption>
                            </figure>
                            <figure>
                                <a href="images/screen_5.png" title="Application dialog"><img alt="Application dialog" src="images/screen_5.png"></a>
                                <figcaption>Handling of unknown activity names</figcaption>
                            </figure>
                        </dd>
                        <dt>Mobile support</dt>
                        <dd>
                            <div class="description">
                                <p>
                                    To broaden the availability of our application even further, we have designed a fully functional mobile version following the modern mobile design standards.
                                </p>
                                <p>
                                    The mobile and the desktop version of our application are <strong>perfectly synchronized</strong> - <em>any action you take</em> will be visible on any instance of <abbr title="For Track's Sake">FTS</abbr>.
                                </p>
                            </div>
                            <figure>
                                <a href="images/screen_6.png" title="Mobile layout"><img alt="Mobile layout" src="images/screen_6.png"></a>
                                <figcaption>Layout of the "Tracking" subpage in the mobile version</figcaption>
                            </figure>
                        </dd>
                    </dl>
                </section>

                <section>
                    <h3>Pricing</h3>
                    <p>
                        To use <abbr title="For Track's Sake">FTS</abbr> you have to be registered. Registration takes 5 seconds and requires your e-mail address and password.
                        No further activation is required. After completing the registration, log in with the information you entered and start tracking. We will never sell or otherwise discredit your e-mail address.
                    </p>

                    <p>
                        <abbr title="For Track's Sake">FTS</abbr> <strong>is</strong> and <strong>will be free</strong> forever.
                    </p>
                </section>
            </div>
        </section>

        <section>
            <span id="appendix"></span>
            <div class="section-content">
                <h2>Appendix</h2>
                <p>
                    At first, it was only supposed to be an ordinary school assignment but our passion for web development and our
                    thirst for knowledge has advanced it to something different. Something we are both proud of. Of course,
                    like every just released product, there's much work to be done and we are not finished in the slightest.
                    However, we believe we have the right mindset and determination to drive it to the finish line and satisfy as
                    much people as possible.
                </p>
            </div>
        </section>
    </article>
    <footer>
        For Track's Sake © Peter Paulovics, Stanislav Krajčovič. 2016. All rights reserved.
    </footer>
</body>
</html>