<?php
/** @var mysqli $DB */
$DB = null;

function db_connect()
{
    global $DB;
    $DB_CONFIG = unserialize(DB_CONFIG);

    $DB = new mysqli($DB_CONFIG['server'],
        $DB_CONFIG['username'],
        $DB_CONFIG['password'],
        $DB_CONFIG['database']);

    $DB->set_charset("utf8");
}

function validate_cookie_user($cookie) {
    global $DB;

    $id = fullyEscape($cookie->id);
    $password = $cookie->password;

    $query = "SELECT * FROM pms_fts_users WHERE id='$id'";
    $query_result = $DB->query($query);

    if (!$query_result){
        return false;
    }

    if ($row = $query_result->fetch_assoc()){
        if ($row['password'] === $password) {
            if (session_status() === PHP_SESSION_NONE) {
                session_start();
            }

            $_SESSION['user'] = ["id" => $row['id'],
                "email" => $row['email'],
                "password" => $row['password'],
                "live_tracking_started" => $row['live_tracking_started'],
                "live_tracking_activity_id" => $row['live_tracking_activity_id'],
                "remember" => $cookie->remember
            ];
            return true;
        }
    }
    return false;
}

function fullyEscape($string){
    global $DB;
    return $DB->escape_string(trim(htmlspecialchars(strip_tags($string))));
}

function is_user_logged_in() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    return isset($_SESSION['user']) || isset($_COOKIE["user"]) && validate_cookie_user(json_decode($_COOKIE["user"]));
}

if (in_array($_SERVER["REMOTE_ADDR"], array('127.0.0.1', '::1'))) {
    require_once "database-local.php";
}
else {
    define("DB_CONFIG", serialize(array(
        'server' => '46.229.230.119',
        'username' => 'rs017100',
        'password' => 'mvabived',
        'database' => 'rs017100db'
    )));
}

db_connect();