<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
require_once "database.php";

if (is_user_logged_in()) {
    header("Location: tracking.php");
    exit();
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>FTS | Log in</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" media="(max-width: 960px)" href="css/mobile.css">

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="js/index.js"></script>
</head>
<body>
<main id="login" class="container">
    <h1 style="margin-bottom: 10px; font-size: 32px;" class="button-wrapper">FTS</h1>
    <section id="login-form">
        <form method="post">

            <div class="errormsg"></div>
            <div class="infomsg"></div>

            <div class="form-group">
                <div class="input-group">
                    <input type="email" name="email" class="inputtext" placeholder="E-mail" maxlength="255" required/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="password" name="password" class="inputtext" placeholder="Password" required/>
                </div>
            </div>

            <div class="form-group" id="remember-anaccount">
                <label>
                    <input type="checkbox" class="remember" name="remember" value="1" checked>
                    Remember me
                </label>
                <a id="register-a" href="#">Register</a>
            </div>

            <div class="button-wrapper form-group">
                <button type="submit" class="btn-green" id="btn-login">LOG IN</button>
            </div>
        </form>
    </section>

    <section id="register-form">
        <form method="post">
            <div class="infomsg"></div>
            <div class="errormsg"></div>

            <div class="form-group">
                <input type="email" name="email" class="inputtext" placeholder="Email address" maxlength="255" required/>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="inputtext" placeholder="Password"
                       title="Enter 4 or more characters" pattern=".{4,}" required/>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="password" name="password2" class="inputtext" placeholder="Password repeat"
                           title="This password field must match the previous one" pattern=".{4,}" required/>
                </div>
                <a id="login-a" href="#">Login</a>
            </div>


            <div class="form-group">
                <div class="button-wrapper">
                    <button type="submit" class="btn-green" id="btn-signup">SIGN UP</button>
                </div>
            </div>
        </form>
    </section>
</main>
</body>
</html>