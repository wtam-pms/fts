$D = {};
var slideUp = $();
var fadeOut = $();
var unloading = false;

const c_user = $.cookie("user");
const user = c_user === undefined ? {id : null, email : null} : $.parseJSON(c_user);

/**--------------------------**/
/**----                  ----**/
/**----    FUNCTIONS     ----**/
/**----                  ----**/
/**--------------------------**/

function page_dim_off(elem) {
    $D.page_dim.fadeOut();
    if (elem !== undefined){
        elem.fadeOut();
    }
    else {
        $(".dim-click-hide").fadeOut();
    }
}

function page_dim_on(elem) {
    $D.page_dim.fadeIn();
    elem.fadeIn();
}

function applyTimeInputMask(){
    $(".time-input").inputmask({alias: "hh:mm",
        placeholder: "00:00",
        clearMaskOnLostFocus: false
    });
}

function createNewActivity(name, onSuccess) {
    ajax({
        data : {
            action : "newActivity",
            user_id : user.id,
            name : name
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            if (data.success) {
                const activity = new Activity(data.activity);
                if (onSuccess !== undefined) {
                    onSuccess(activity);
                }
            }
            else {
                alert(data.msg);
            }
        }
    }, true);
}

function ajax(options, silent) {
    if (silent != true) {
        $D.page_dim.isCloseable = false;
        page_dim_on($D.ajax_anim);
        if (options.complete !== undefined) {
            const orig = options.complete;
            options.complete = function (jqXHR, textStatus) {
                orig(jqXHR, textStatus);
                $D.page_dim.isCloseable = true;
                page_dim_off($D.ajax_anim);
            }
        }
        else {
            options.complete = function (jqXHR, textStatus) {
                $D.page_dim.isCloseable = true;
                page_dim_off($D.ajax_anim);
                $D.ajax_anim.fadeOut();
            }
        }
    }

    if (!unloading) {
        if (options.error !== undefined) {
            const orig = options.error;
            options.error = function (jqXHR, textStatus) {
                orig(jqXHR, textStatus);
                console.error("AJAX ERROR: " + jqXHR.responseText + " (" + textStatus + ")");
            }
        }
        else {
            options.error = function (jqXHR, textStatus) {
                console.error("AJAX ERROR: " + jqXHR.responseText + " (" + textStatus + ")");
                console.error(options.data);
            }
        }
    }

    options["cache"] = false;
    options["method"] = "POST";

    $.ajax("ajax.php", options);
}

/**--------------------------**/
/**----                  ----**/
/**----     CLASSES      ----**/
/**----                  ----**/
/**--------------------------**/

Object.defineProperty(Object.prototype, "hasOwnPropertyCI", {
    value: function (prop) {
        var key,self = this;
        for (key in self) {
            if (key.toLowerCase() == prop.toLowerCase()) {
                return true;
            }
        }
        return false;
    },
    enumerable: false
});

if (Object.values === undefined) {
    Object.values = function (object) {
        return Object.keys(object).map(function (key) {
            return object[key];
        });
    };
}

const BaseObject = function (options) {
    for (var key in options){
        if (options.hasOwnProperty(key)){
            this[key] = options[key];
        }
    }
};

/**--------------------------**/
/**----                  ----**/
/**----     CALENDAR     ----**/
/**----                  ----**/
/**--------------------------**/

const Calendar = function (options) {
    this.mode = Calendar.modes.DAY;
    this.dateFrom = moment();
    this.dateTo = moment();
    BaseObject.call(this, options);

    const self = this;

    this.getText = function () {
        return this.dateFrom.format("Do MMMM Y");
    };

    this.setRange = function (momentFrom, momentTo) {
        this.dateFrom = momentFrom;
        this.dateTo = momentTo;

        self.onRangeChange(this.dateFrom, this.dateTo);
    };

    this.onRangeChange = function(momentFrom, momentTo){};

    this.isToday = function () {
        return calendar.dateFrom.format("YYYY-MM-DD") === moment().format("YYYY-MM-DD");
    }
};

Calendar.modes = {
    DAY : 0,
    WEEK : 1,
    MONTH : 2
};

const calendar = new Calendar();

/**--------------------------**/
/**----                  ----**/
/**----     ACTIVITY     ----**/
/**----                  ----**/
/**--------------------------**/

const Activity = function (options) {
    this.id = null;
    this.name = null;
    this.tile = null;
    this.color = null;
    BaseObject.call(this, options);

    const self = this;

    if (self.name === "Not assigned")
        Activity.default = self;

    this.getTile = function () {
        if (self.tile === null) {
            self.tile = $("<li class='activity-tile'>" +
                "<div style='height: 100%'>" +
                "<div class='activity-color'></div>" +
                "<h3>" + self.name + "</h3></div>" + (self === Activity.default ? "" :
                "<i class='fa fa-ellipsis-v activity-menu-btn' aria-hidden='true'></i>") +
                "</li>");

            self.tile.find(".activity-color").css('background-color', this.color);

            if (self === Activity.default) {
                self.tile.css('opacity', 0);
                self.tile.css('position', 'absolute');
            }

            self.tile.find(".activity-color").click(function (event) {
                $D.activity_context_menu.activity = self;
                $D.sp_control.spectrum("set", $D.activity_context_menu.activity.color);
                page_dim_on($D.sp_container);
                $D.activity_context_menu.fadeOut();
            });

            self.tile.find(".activity-menu-btn").click(function (event) {
                if ($D.activity_context_menu.activity !== self) {
                    $D.activity_context_menu.activity = self;
                    $D.activity_context_menu.hide();

                    const offset = self.tile.offset();
                    $D.activity_context_menu.css('top', offset.top+self.tile.outerHeight());
                    $D.activity_context_menu.css('left', offset.left+self.tile.outerWidth()-$D.activity_context_menu.outerWidth());
                }

                $D.activity_context_menu.fadeToggle();
                event.stopPropagation();
            });
        }
        return self.tile;
    };

    this.setName = function (name) {
        name = name.trim();

        const index = Activity.names.indexOf(self.name);
        if (index != -1){
            Activity.names[index] = name;
        }
        else {
            Activity.names.push(name);
        }

        self.name = name;
        self.getTile().find("h3").text(name);

        return true;
    };

    this.setColor = function(color) {
        self.color = color;
        self.getTile().find(".activity-color").css('background-color', color);
    };

    this.getFadedColor = function () {
        if (self === Activity.default)
            return self.color;

        const color = tinycolor(self.color).toHsl();
        color.s = Math.min(color.s, 0.25);
        color.l = Math.max(color.l, 0.75);
        return tinycolor(color).toHexString();
    };

    Activity.add(this);
};

Activity.activities = [];
Activity.names = [];

Activity.getAll = function () {
    return Object.values(Activity.activities);
};

Activity.getByName = function (name) {
    name = name.trim().toLowerCase();
    for (var id in Activity.activities) {
        if(Activity.activities.hasOwnProperty(id)) {
            if (Activity.activities[id].name.toLowerCase() === name){
                return Activity.activities[id];
            }
        }
    }
    return null;
};

Activity.getAllSorted = function () {
    return Activity.getAll().sort(function(a, b) {
        const  nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
        if (nameA < nameB)
            return -1;
        if (nameA > nameB)
            return 1;
        return 0;
    });
};

Activity.getNames = function () {
    return Activity.names;
};

Activity.add = function (activity) {
    Activity.activities[activity.id] = activity;
    Activity.names.push(activity.name);
    if (Activity.reinsert !== undefined){
        Activity.reinsert(activity);
    }
    return true;
};

Activity.onLoad = function () {
    calendar.onRangeChange(calendar.dateFrom, calendar.dateTo);
};

Activity.loadAll = function () {
    ajax({
        data : {
            action : "getAllActivities",
            user_id : user.id
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR ) {
            $.each(data, function (i, o) {
                new Activity(o);
            });
            Activity.onLoad();
        }
    });
};


/**--------------------------**/
/**----                  ----**/
/**----      RECORD      ----**/
/**----                  ----**/
/**--------------------------**/

const Record = function (options) {
    this.id = null;
    this.activity = Activity.default;
    this.trackTile = null;
    this.reportTile = null;
    const now = new Date();
    this.date = [now.getDate(), now.getMonth()+1, now.getFullYear()];
    this.begin = Record.getAll().length === 0 ? "00:00" : Record.getAll()[Record.getAll().length-1].end;
    this.end = now.toTimeString().slice(0,5);

    if (options.activity_id !== undefined){
        options.activity = Activity.activities[options.activity_id];
        delete options.activity_id;
    }

    BaseObject.call(this, options);

    if (typeof this.date === "string"){
        this.date = this.date.split("-");
        this.date.reverse();
    }

    if (this.activity === undefined){
        this.activity = Activity.default;
    }

    const self = this;

    this.delete = function () {
        Record.delete(self);
        self.getTrackTile().fadeOut({
            complete : self.getTrackTile().remove
        });
    };

    this.getTrackTile = function () {
        if (self.trackTile === null) {
            self.trackTile = $("<div class='record-track-tile'>" +
                "<div><input class='awesomplete activity-name-input' value='"+self.activity.name+"'/></div>" +
                "<div><input class='time-input begin-time-input' value='"+self.begin+"'></div>" +
                "<div><input class='time-input end-time-input' value='"+self.end+"'></div>" +
                "<div><i class='fa fa-times record-delete-btn' aria-hidden='true'></i></div>" +
                "</div>");

            const activity_name_input = self.trackTile.find('.activity-name-input');

            new Awesomplete(activity_name_input[0], {list: Activity.names, minChars : 1, maxItems : 5});

            activity_name_input.on('awesomplete-selectcomplete input', function () {
                var activity_name = $(this).val().trim();

                if (activity_name === "") {
                    activity_name = Activity.default.name;
                }

                var activity = Activity.getByName(activity_name);

                if (activity === null) {
                    activity = Activity.default;
                }

                self.activity = activity;
                self.trackTile.css('background-color', self.activity.getFadedColor());
            });

            activity_name_input.on('awesomplete-selectcomplete', function () {
                setRecordActivity(self, $(this).val());
            });


            setUpInputAjax(activity_name_input, function () {
                setRecordActivity(self, activity_name_input.val());
            });

            const begin_time_input = self.trackTile.find(".begin-time-input");
            const end_time_input = self.trackTile.find(".end-time-input");

            begin_time_input.on("input", function () {
                self.begin = begin_time_input.val();
            });

            setUpInputAjax(begin_time_input, function () {
                setRecordBeginTime(self);
            });

            end_time_input.on("input", function () {
                self.end = end_time_input.val();
            });

            setUpInputAjax(end_time_input, function () {
                setRecordEndTime(self);
            });

            self.trackTile.find(".record-delete-btn").click(function (event) {
                if (confirm("Do you really want to delete this record?\n\n" +
                        "Activity: " + self.activity.name + "\n" +
                        "Begin: " + self.begin + "\n" +
                        "End: " + self.end))
                    ajax({
                        data : {
                            action : "deleteRecord",
                            user_id : user.id,
                            record_id : self.id
                        },
                        success : function (data, textStatus, jqXHR) {
                            self.delete();
                        }
                    }, true);
            });

            self.trackTile.find(".time-input").inputmask({alias: "hh:mm", placeholder: "00:00", clearMaskOnLostFocus: false});

            self.trackTile.css('background-color', self.activity.getFadedColor());
        }
        return self.trackTile;
    };

    this.setActivity = function (activity) {
        self.activity = activity;

        if (self.trackTile !== null){
            self.trackTile.find(".activity-name-input").val(activity.name);
            self.trackTile.css('background-color', self.activity.getFadedColor());
        }
    };

    this.setTimeSpan = function (begin, end) {
        self.begin = begin;
        self.end = end;

        if (self.trackTile !== null){
            self.trackTile.find(".begin-time-input").val(begin);
            self.trackTile.find(".end-time-input").val(end);
        }

        if (Record.reinsert !== undefined){
            Record.reinsert(self);
        }
    };

    Record.add(this);
};

Record.records = {};

Record.getAll = function () {
    return [].concat.apply([], Object.values(Record.records));
};

Record.get = function (date) {
    if (Record.records.hasOwnProperty(date)){
        return Record.records[date];
    }
    else {
        return [];
    }
};

Record.getSorted = function (date) {
    return Record.get(date).sort(function(a, b) {
        const a_begin = parseInt(a.begin.slice(0,2) + a.begin.slice(3)),
            b_begin = parseInt(b.begin.slice(0,2) + b.begin.slice(3));
        if (a_begin < b_begin)
            return -1;
        if (a_begin > b_begin)
            return 1;

        const a_end = parseInt(a.end.slice(0,2) + a.end.slice(3)),
            b_end = parseInt(b.end.slice(0,2) + b.end.slice(3));
        if (a_end < b_end)
            return -1;
        if (a_end > b_end)
            return 1;
        return 0;
    });
};

Record.add = function (record) {
    if (!Record.records.hasOwnProperty(record.date)){
        Record.records[record.date] = [];
    }

    Record.records[record.date].push(record);

    if (Record.reinsert !== undefined){
        Record.reinsert(record);
    }
};

Record.delete = function (record) {
    const array = Record.get(record.date);
    const index = array.indexOf(record);
    if (index != -1){
        Record.records[record.date].splice(index, 1);
    }
};

Record.loadRange = function (momentFrom, momentTo) {
    ajax({
        data : {
            action : "getRecordsRange",
            user_id : user.id,
            date_from : momentFrom.format("YYYY-MM-DD"),
            date_to : momentTo.format("YYYY-MM-DD")
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR ) {
            $(".record-track-tile").remove();
            $(".record-report-tile").remove();
            delete Record.records;

            Record.records = {};
            $.each(data, function (i, o) {
                new Record(o);
            });
        }
    });
};

/**--------------------------**/
/**----                  ----**/
/**----    DOM READY     ----**/
/**----                  ----**/
/**--------------------------**/

$(function () {
    $D.page_dim = $("#page-dim");
    $D.page_dim.isCloseable = true;
    $D.ajax_anim = $("#ajax-anim");
    $D.date_range = $("#date-range");
    $D.date_picker = $("#date-picker");
    $D.helper_popups = $(".helper-popups");
    $D.logout_btn = $("#logout");

    /**--------------------------**/
    /**----                  ----**/
    /**----      HIDING      ----**/
    /**----                  ----**/
    /**--------------------------**/

    $D.page_dim.hide();
    $D.ajax_anim.hide();
    $D.date_picker.hide();


    /**--------------------------**/
    /**----                  ----**/
    /**----  FUNCTION CALLS  ----**/
    /**----                  ----**/
    /**--------------------------**/

    Activity.loadAll();

    $D.date_range.text(calendar.getText());

    applyTimeInputMask();

    /**--------------------------**/
    /**----                  ----**/
    /**----      EVENTS      ----**/
    /**----                  ----**/
    /**--------------------------**/

    $("a").click(function (event) {
        a = $(this)[0];
        if (a.href.trim() !== ""){
            event.preventDefault();
            window.location.href = a.href;
        }
    });

    $D.logout_btn.click(function () {
       ajax({
           data : {action : "logOut"},
           complete : function () {
               document.location.href=".";
           }
       }, true)
    });

    $D.page_dim.click(function () {
        if ($D.page_dim.isCloseable)
            page_dim_off();
    });

    $(document).on("focus", "input", function () {
        $(this).select();
    });

    $(document).keydown(function(e) {
        if (e.keyCode == 27) {
            $D.page_dim.click();
        }
    });

    slideUp = slideUp.add($D.date_picker);
});