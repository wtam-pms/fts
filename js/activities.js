Activity.reinsert = function(activity){
    const sorted = Activity.getAllSorted();

    for (var i = 0; i < sorted.length; i++) {
        if (sorted[i] === activity)   {
            if (i < sorted.length-1){
                sorted[i+1].getTile().before(activity.getTile());
            }
            else {
                $D.activity_list.append(activity.getTile());
            }
            activity.getTile().hide().slideDown();
        }
    }
};

/**--------------------------**/
/**----                  ----**/
/**----    FUNCTIONS     ----**/
/**----                  ----**/
/**--------------------------**/

function filter_activities() {
    $D.activity_list.children().each(function () {
        if ($(this).find("h3").text().toLowerCase().indexOf($D.activity_filter.val().toLowerCase()) >= 0){
            $(this).slideDown();
        }
        else {
            $(this).slideUp();
        }
    })
}

function reset_filter() {
    $D.activity_filter.val("");
    filter_activities();
}

function setActivityName(activity, name, onSuccess) {
    ajax({
        data : {
            action : "setActivityName",
            user_id : user.id,
            activity_id : activity.id,
            name : name
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            if (data.success){
                activity.setName(name);

                if (onSuccess !== undefined){
                    onSuccess();
                }
            }
            else {
                alert(data.msg);
            }
        }
    }, true);
}

function setActivityColor(activity, color, onSuccess) {
    ajax({
        data : {
            action : "setActivityColor",
            user_id : user.id,
            activity_id : activity.id,
            color : color.substring(1)
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            if (data.success){
                activity.setColor(color);

                if (onSuccess !== undefined){
                    onSuccess();
                }
            }
            else {
                alert(data.msg);
            }
        }
    }, true);
}

/**--------------------------**/
/**----                  ----**/
/**----    DOM READY     ----**/
/**----                  ----**/
/**--------------------------**/

$(function () {
    $D.activity_list = $(".activity-list");
    $D.activity_filter = $(".activity-filter");
    $D.new_activity = $(".new-activity");
    $D.new_activity_modal = $("#new-activity-modal");
    $D.new_activity_input = $D.new_activity_modal.find("input");
    $D.new_activity_button = $D.new_activity_modal.find("button");
    $D.activity_rename_modal = $("#activity-rename-modal");
    $D.activity_rename_input = $D.activity_rename_modal.find("input");
    $D.activity_rename_button = $D.activity_rename_modal.find("button");
    $D.activity_context_menu = $("#activity-context-menu");
    $D.sp_control = $("#sp-control");

    /**--------------------------**/
    /**----                  ----**/
    /**----      HIDING      ----**/
    /**----                  ----**/
    /**--------------------------**/

    $D.new_activity_modal.hide();
    $D.activity_rename_modal.hide();
    $D.activity_context_menu.hide();

    /**--------------------------**/
    /**----                  ----**/
    /**----  FUNCTION CALLS  ----**/
    /**----                  ----**/
    /**--------------------------**/

    $D.activity_context_menu.menu();

    $D.sp_control.spectrum({
        flat: true,
        showInput: false,
        preferredFormat: "hex"
    });

    $D.sp_container = $(".sp-container");
    $D.sp_container.hide();
    $D.sp_container.addClass("dim-click-hide");

    /**--------------------------**/
    /**----                  ----**/
    /**----      EVENTS      ----**/
    /**----                  ----**/
    /**--------------------------**/

    $D.activity_filter.on('input', function () {
        filter_activities();
    });

    $D.activity_filter.on('keydown', function (e) {
        if (e.keyCode === 13 || e.keyCode === 27){
            $D.activity_filter.blur()
        }
    });

    $D.new_activity.click(function () {
        $D.new_activity_modal.fadeIn();
        $D.new_activity_input.focus();
        $D.page_dim.fadeIn();
    });

    $D.new_activity_input.keydown(function (event) {
        if (event.keyCode === 13){
            createNewActivity($D.new_activity_input.val(), function () {
                page_dim_off();
                reset_filter();
                $D.new_activity_input.val("");
            });
        }
    });

    $D.new_activity_button.click(function() {
        createNewActivity($D.new_activity_input.val(), function () {
            page_dim_off();
            reset_filter();
            $D.new_activity_input.val("");
        });
    });

    $D.activity_context_menu.find("li:first-child").click(function () {
        page_dim_on($D.activity_rename_modal);
        $D.activity_rename_input.val($D.activity_context_menu.activity.name);
        $D.activity_rename_input.focus();
        $D.activity_rename_input.select();
        $D.activity_context_menu.fadeOut();
    });

    $D.activity_rename_input.keydown(function (event) {
        if (event.keyCode === 13) {
            setActivityName($D.activity_context_menu.activity, $D.activity_rename_input.val(), function () {
                Activity.reinsert($D.activity_context_menu.activity);
                page_dim_off();
            });
        }
    });

    $D.activity_rename_button.click(function () {
        setActivityName($D.activity_context_menu.activity, $D.activity_rename_input.val(), function () {
            Activity.reinsert($D.activity_context_menu.activity);
            page_dim_off();
        });
    });

    $D.activity_context_menu.find("li:nth-child(2)").click(function () {
        $D.sp_control.spectrum("set", $D.activity_context_menu.activity.color);
        page_dim_on($D.sp_container);
        $D.activity_context_menu.fadeOut();
    });

    $D.sp_container.find(".sp-choose").click(function () {
        setActivityColor($D.activity_context_menu.activity, $D.sp_control.spectrum("get").toHexString(), function () {
            page_dim_off();
        });
    });

    fadeOut = fadeOut.add($D.activity_context_menu);
});