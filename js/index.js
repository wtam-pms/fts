$(function () {
    const login_form = $("#login-form"),
    login_email = login_form.find("[name=email]"),
    login_password = login_form.find("[name=password]"),
    login_remember = login_form.find("[name=remember]"),
    login_submit = login_form.find("#btn-login"),
    login_error_msg = login_form.find(".errormsg"),
    login_info_msg = login_form.find(".infomsg"),
    register_form = $("#register-form"),
    register_email = register_form.find("[name=email]"),
    register_password = register_form.find("[name=password]"),
    register_password2 = register_form.find("[name=password2]"),
    register_submit = register_form.find("#btn-signup"),
    register_error_msg = register_form.find(".errormsg"),
    register_info_msg = register_form.find(".infomsg");

    login_submit.click(function () {
        $.ajax("ajax.php", {
            cache : false,
            method : "POST",
            data : {
                action : "logIn",
                email : login_email.val(),
                password : login_password.val(),
                remember : login_remember.is(':checked')
            },
            dataType : "JSON",
            success : function (data, textStatus, jqXHR ) {
                login_error_msg.text("");
                login_info_msg.text("");

                if (data.success){
                    login_info_msg.text(data.msg);
                    location.reload();
                }
                else {
                    login_error_msg.text(data.msg);
                }
            },
            error : function (jqXHR, textStatus, errorThrown){
                login_info_msg.text("");
                login_error_msg.text("A server error has occured.");
            }
        });

        return false;
    });

    register_submit.click(function () {
        $.ajax("ajax.php", {
            cache : false,
            method : "POST",
            data : {
                action : "register",
                email : register_email.val(),
                password : register_password.val(),
                password2 : register_password2.val()
            },
            dataType : "JSON",
            success : function (data, textStatus, jqXHR ) {
                register_error_msg.text("");
                register_info_msg.text("");

                if (data.success){
                    register_info_msg.text(data.msg);
                }
                else {
                    register_error_msg.text(data.msg);
                }
            },
            error : function (jqXHR, textStatus, errorThrown){
                login_info_msg.text("");
                register_error_msg.text("A server error has occured.");
            }
        });

        return false;
    });

    register_form.hide();

    $("#register-a").on('click', function() {
        login_form.slideUp();
        register_form.fadeIn();
    });

    $("#login-a").on('click', function() {
        register_form.slideUp();
        login_form.slideDown();
    });
});