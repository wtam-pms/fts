var live_track_interval;
var live_track_started;
var lastTimeHandled = 0;
var unloadHandler = null;

Record.reinsert = function(record){
    const sorted = Record.getSorted(record.date);

    for (var i = 0; i < sorted.length; i++) {
        if (sorted[i] === record)   {
            if (i < sorted.length-1){
                sorted[i+1].getTrackTile().before(record.getTrackTile());
            }
            else {
                $D.record_table.append(record.getTrackTile());
            }
            record.getTrackTile().hide().slideDown();
        }
    }
};

/**--------------------------**/
/**----                  ----**/
/**----    FUNCTIONS     ----**/
/**----                  ----**/
/**--------------------------**/

function setUpInputAjax(input, ajaxHandler) {
    input.focus(function () {
            unloadHandler = ajaxHandler;
    });
    input.on("keydown", function (event) {
        if (event.keyCode === 13) {
            if (Date.now() > lastTimeHandled + 100) {
                lastTimeHandled = Date.now();
                input.blur();
                ajaxHandler();
            }
        }
    });
    input.blur(function () {
        if (unloadHandler === ajaxHandler){
            unloadHandler = null;
        }

        if (Date.now() > lastTimeHandled + 100) {
            lastTimeHandled = Date.now();
            ajaxHandler();
        }
    });
}

function setLiveTrackingActivity() {
    const activity_name = $D.live_activity.val().trim();

    ajax({
        data : {
            action : "setLiveTrackingActivity",
            user_id : user.id,
            activity_name : activity_name
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            if (!data.success) {
                if (confirm("There is no activity named '" + activity_name + "'. Would you like to create a new one?")) {
                    createNewActivity(activity_name, function (activity) {
                        setLiveTrackingActivity();
                    });
                }
            }
        }
    }, true);
}

function setRecordTime(record, ajax_function) {
    ajax({
        data : {
            action : ajax_function,
            user_id : user.id,
            id : record.id,
            date : record.date[2]+"-"+record.date[1]+"-"+record.date[0],
            begin : record.begin,
            end : record.end,
            upper_limit : calendar.isToday() ? moment().format("HH:mm") : null
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            record.setTimeSpan(data.begin, data.end);
        }
    }, true);
}

function setRecordBeginTime(record) {
    setRecordTime(record, "setRecordBeginTime");
}

function setRecordEndTime(record) {
    setRecordTime(record, "setRecordEndTime");
}

function setRecordActivity(record, activity_name) {
    if ((activity_name).trim() === ""){
        activity_name = Activity.default.name;
    }
    ajax({
        data : {
            action : "setRecordActivity",
            user_id : user.id,
            id : record.id,
            activity_name : activity_name
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            var activity = null;
            if (data === null){
                if (confirm("There is no activity named '"+activity_name+"'. Would you like to create a new one?")){
                    createNewActivity(activity_name, function (activity) {
                        record.setActivity(activity);
                    });
                    return;
                }
            }
            else {
                activity = Activity.activities[data.id];
                if (activity === undefined){
                    new Activity(data);
                }
            }
            if (activity === null){
                activity = Activity.default;
            }
            record.setActivity(activity);
        }
    }, true);
}

function startLiveTracking() {
    live_track_started = moment();

    ajax({
        data : {
            action : "startLiveTracking",
            user_id : user.id,
            datetime : live_track_started.format("YYYY-MM-DD HH:mm:ss")
        },
        success : function (data, textStatus, jqXHR) {
            fetchLiveTracking(true);
        }
    });
}

function fetchLiveTracking(silent) {
    ajax({
        data : {
            action : "fetchLiveTracking",
            user_id : user.id
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR) {
            if (live_track_interval !== undefined) {
                clearInterval(live_track_interval);
                live_track_interval = undefined;
            }
            if (data.begin === null) {
                $D.live_details.hide();
                $D.live_btn.removeClass("live-stop");
                $D.live_btn.addClass("live-start");
                $D.live_btn.html('<i class="fa fa-play" aria-hidden="true"></i> Start');
                $D.live_activity.val("");
                live_track_started = undefined;
            }
            else {
                live_track_started = moment(data.begin, "YYYY-MM-DD HH:mm:ss");
                $D.live_activity.val(data.name);
                $D.live_btn.removeClass("live-start");
                $D.live_btn.addClass("live-stop");
                $D.live_btn.html('<i class="fa fa-stop" aria-hidden="true"></i> Stop');
                $D.live_time.text("0s");
                $D.live_begin.text(live_track_started.format("Do MMM Y HH:mm"));

                live_track_interval = setInterval(function handler() {
                    var diff = moment().diff(live_track_started, 'seconds');
                    var to_display = [];
                    to_display[0] = Math.floor(diff / 86400);
                    diff %= 86400;
                    to_display[1] = Math.floor(diff / 3600);
                    diff %= 3600;
                    to_display[2] = Math.floor(diff / 60);
                    diff %= 60;
                    to_display[3] = diff;

                    var result = "";
                    if (to_display[0] > 0) {
                        result += to_display[0] + " day" + (to_display[0] === 1 ? "" : "s") + " ";
                    }
                    if (result !== "" || to_display[1] > 0) {
                        result += to_display[1] + "h ";
                    }
                    if (result !== "" || to_display[2] > 0) {
                        result += to_display[2] + "m ";

                        if (to_display[0] === 0 && to_display[1] === 0) {
                            result += to_display[3] + "s";
                        }
                    }
                    if (result === "") {
                        result = to_display[3] + "s";
                    }
                    $D.live_time.text(result);

                    return handler;
                }(), 1000);

                $D.live_details.show();
            }
        }
    }, silent);
}

function stopLiveTracking() {
    ajax({
        data: {
            action: "stopLiveTracking",
            user_id: user.id,
            datetime : moment().format("YYYY-MM-DD HH:mm")
        },
        success: function (data, textStatus, jqXHR) {
            fetchLiveTracking(true);
            Record.loadRange(calendar.dateFrom, calendar.dateTo);
        }
    });
}

function editActivity(activity) {
    $("body > header nav > a").removeClass("active");
    $("body > header nav > a:nth-child(4)").addClass("active");
    $D.activity_filter.val(activity.name);
    filter_activities();
}


/**--------------------------**/
/**----                  ----**/
/**----    DOM READY     ----**/
/**----                  ----**/
/**--------------------------**/

$(function () {
    $D.date_picker_trigger = $(".date-picker-trigger");
    $D.record_table = $("#record-table");
    $D.record_add = $(".record-add");
    $D.live_btn = $("#live-tracking").find("button");
    $D.live_details = $("#live-tracking").find(".live-details");
    $D.live_begin = $D.live_details.find(".live-begin");
    $D.live_activity = $D.live_details.find(".live-activity");
    $D.live_time = $D.live_details.find(".live-time");
    $D.record_context_menu = $("#record-context-menu");

    /**--------------------------**/
    /**----                  ----**/
    /**----      HIDING      ----**/
    /**----                  ----**/
    /**--------------------------**/

    if ($D.live_btn.hasClass("live-start"))
        $D.live_details.hide();

    $D.record_context_menu.hide();

    /**--------------------------**/
    /**----                  ----**/
    /**----  FUNCTION CALLS  ----**/
    /**----                  ----**/
    /**--------------------------**/

    $D.date_picker_trigger.daterangepicker({
        locale: {
            firstDay : 1
        },
        singleDatePicker : true,
        startDate : moment(),
        maxDate: moment()
    }, function (start, end, label) {
        calendar.setRange(start, end);
    });

    $("input.awesomplete").each(function (i, o) {
        new Awesomplete(o, {list : Activity.names, minChars : 1, maxItems : 5,
            sort : function (a, b) {
                a = a.toLowerCase();
                b = b.toLowerCase();
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0;
        }});
    });

    $D.record_context_menu.menu();

    fetchLiveTracking();

    /**--------------------------**/
    /**----                  ----**/
    /**----      EVENTS      ----**/
    /**----                  ----**/
    /**--------------------------**/

    setUpInputAjax($D.live_activity, setLiveTrackingActivity);
    $D.live_activity.on('awesomplete-selectcomplete', function () {
        setLiveTrackingActivity();
    });

    calendar.onRangeChange = function (momentFrom, momentTo) {
        $D.date_range.text(calendar.getText());
        Record.loadRange(momentFrom, momentTo);
    };

    $D.live_btn.click(function () {
        if ($D.live_btn.hasClass("live-start")){
            startLiveTracking();
        }
        else {
            stopLiveTracking();
        }
    });

    $D.record_add.click(function () {
        ajax({
            data : {
                action : "newRecord",
                user_id : user.id,
                date : calendar.dateFrom.format("YYYY-MM-DD"),
                end : calendar.isToday() ? moment().format("HH:mm") : "23:59"
            },
            dataType : "JSON",
            success : function (data, textStatus, jqXHR) {
                new Record(data);
            }
        });
    });

    window.onbeforeunload = function () {
        unloading = true;
        if (unloadHandler !== null){
            unloadHandler();
        }
    };

    fadeOut = fadeOut.add($D.record_context_menu);
});