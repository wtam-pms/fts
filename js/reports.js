var startD;
var endD;

var dChart;
var bChart;

var barChartDataGlobal;
var donutChartDataGlobal;

Activity.onLoad = function () {
    createCharts(startD, endD);
};

function createDonutChart(data) {
    var dtx = document.getElementById("d_chart");
    return new Chart(dtx, {
        type: 'doughnut',
        data: data
    });
}

function createBarChart(data) {
    var bcx = document.getElementById("bar_chart");
    return new Chart(bcx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function calculateDuration(t1, t2) {
    var time1 = t1.split(':'), time2 = t2.split(':');
    var hours1 = parseInt(time1[0], 10),
        hours2 = parseInt(time2[0], 10),
        mins1 = parseInt(time1[1], 10),
        mins2 = parseInt(time2[1], 10);
    var hours = hours2 - hours1, mins = 0;

    if(hours < 0) hours = 24 + hours;

    if(mins2 >= mins1) {
        mins = mins2 - mins1;
    }
    else {
        mins = (mins2 + 60) - mins1;
        hours--;
    }
    mins = mins / 60;
    hours += mins;
    hours = hours.toFixed(2);
    return hours;
}

function isInArray(value, array) {
    return array.indexOf(value) > -1;
}

function insertInto(jobject, data) {
    jobject.labels.push(
        data.name
    );
    jobject.datasets[0].data.push(
        calculateDuration(data.begin, data.end)
    );
    jobject.datasets[0].backgroundColor.push(
        "#" + data.color
    );
}

function createCharts(startDate, endDate) {
    $.ajax("ajax.php", {
        cache : false,
        method : "POST",
        data : {
            action : "pantyDropperMonster",
            user_id: user.id,
            date_from: startDate,
            date_to: endDate
        },
        dataType : "JSON",
        success : function (data, textStatus, jqXHR ) {
            var donutChartData = {};
            donutChartData.labels = [];
            donutChartData.datasets = [];
            donutChartData.datasets[0] = {};
            donutChartData.datasets[0].data = [];
            donutChartData.datasets[0].backgroundColor = [];
            var barChartData = jQuery.extend(true, {}, donutChartData);
            for (var i = 0; i < data.length; i++) {
                var dataLength = parseFloat(calculateDuration(data[i].begin, data[i].end));
                if (dataLength != 0) {
                    insertInto(donutChartData, data[i]);
                    if (isInArray(data[i].name, barChartData.labels)) {
                        var currentValue = barChartData.datasets[0].data[barChartData.labels.indexOf(data[i].name)]
                        currentValue = (parseFloat(currentValue) + dataLength)
                            .toString();
                    }
                    else {
                        insertInto(barChartData, data[i]);
                    }
                }
            }

            barChartData.datasets[0].label = "Number of hours";

            if (dChart && bChart != null) {
                dChart.destroy();
                bChart.destroy();
            }
            dChart = createDonutChart(donutChartData);
            bChart = createBarChart(barChartData);

            createTable(barChartData);
        }
    });
}

function createTable(data) {
    var table = $('#record-table');
    var tableCode = "";
    tableCode += "<div><h4 style='white-space: pre-wrap;'>Activity name</h4><h4 style='white-space: pre-wrap;'>Total duration</h4><h4 style='white-space: pre-wrap;'>% of " + data.labels.length + " days </h4></div>";
    for (var i = 0; i < data.labels.length; i++) {
        // console.log(data.labels[i]);
        tableCode +=
            "<div class='record-track-tile' style='background-color:" + Activity.getByName(data.labels[i]).getFadedColor() + ";'>" +
            "<div>" + data.labels[i] + "</div>" +
            "<div>" + data.datasets[0].data[i] + "h</div>" +
            "<div>" + (data.datasets[0].data[i] / data.labels.length * 10).toFixed(2) + "% </div>" +
            "</div>";
    }
    table.html(tableCode);
}

$(function () {
    startD = moment().format('YYYY-MM-DD');
    endD = moment().format('YYYY-MM-DD');

    var date_range_picker_trigger = $('.date-range-picker-trigger');
    var date_range_reports = $('#date-range-reports');

    date_range_reports.text(calendar.getText());

    date_range_picker_trigger.daterangepicker({
        locale: {
            firstDay : 1
        },
        "startDate" : moment()
    }, function (start, end, label) {
        calendar.setRange(start, end);
        startD = start.format('YYYY-MM-DD');
        endD = end.format('YYYY-MM-DD');
        if (startD == endD) {
            date_range_reports.text(start.format('Do MMMM Y'));
        }
        else {
            date_range_reports.text(start.format('Do MMMM Y') + " - " + end.format('Do MMMM Y'));
        }
        createCharts(startD, endD);
    });
});