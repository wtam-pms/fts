<?php

require_once "common.php";

head($C_TITLES[1]);

?>
    <script src="bower_components/chart.js/dist/Chart.min.js"></script>

    <section>
        <header id="reports-date-download" class="clear-after">
            <h2 class="date-range-picker-trigger"><i class="fa fa-calendar" aria-hidden="true"></i>
                <span id="date-range-reports"></span></h2>
<!--            <button class="download btn-blue">-->
<!--                <i class="fa fa-download" aria-hidden="true"></i> Download PDF-->
<!--            </button>-->
        </header>
    </section>

    <section id="progress">
        <h3>
            Progress
        </h3>

        <canvas id="d_chart" height="100"></canvas>
    </section>

    <section id="overview">
        <h3>
            Overview
        </h3>

        <div>
            <canvas id="bar_chart" height="100"></canvas>
        </div>
    </section>

    <section id="list-of-activities">
        <h3>
            List of activities
        </h3>

        <div id="record-table"></div>
    </section>
<?php
footer();
?>