<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);

require_once "database.php";

function returnJSONStatus($success, $msg) {
    $result["success"] = $success;
    $result["msg"] = $msg;
    echo json_encode($result);
    exit();
}

function isLater($time1, $time2) {
    $len = min(strlen($time1), strlen($time2));
    for ($i = 0; $i < $len; $i++) {
        if ($time1[$i] > $time2[$i] ){
            return true;
        }
        else if ($time1[$i] < $time2[$i]){
            return false;
        }
    }
    return false;
}

switch ($_POST["action"]) {
    case "logIn" :
        $email = fullyEscape($_POST["email"]);
        $password = md5($_POST["password"]);

        $query = "SELECT * FROM pms_fts_users WHERE email='$email'";
        $query_result = $DB->query($query);

        if (!$query_result){
            returnJSONStatus(false, "A database error has occured.");
        }

        if ($row = $query_result->fetch_assoc()){
            if ($row['password'] === $password) {
                if (session_status() === PHP_SESSION_NONE) {
                    session_start();
                }

                $_SESSION['user'] = ["id" => $row['id'],
                    "email" => $row['email'],
                    "password" => $row['password'],
                    "live_tracking_started" => $row['live_tracking_started'],
                    "live_tracking_activity_id" => $row['live_tracking_activity_id'],
                    "remember" => $_POST['remember']
                ];

                returnJSONStatus(true, "You have been logged in.");
            }
            else {
                returnJSONStatus(false, "The password you have entered is incorrect.");
            }
        }
        else {
            returnJSONStatus(false, "There is no account registered with this email.");
        }

        break;
    case "logOut" :
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        setcookie("user", "", 0, "/");
        session_destroy();
        session_unset();
        exit();
        break;
    case "register" :
        $email = fullyEscape($_POST["email"]);

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false){
            returnJSONStatus(false, "Please enter a valid email.");
        }

        $password = $_POST["password"];
        $password2 = $_POST["password2"];

        if (strlen($password) < 4){
            returnJSONStatus(false, "The password should be at least 4 characters long.");
        }

        if ($password !== $password2){
            returnJSONStatus(false, "Passwords do not match.");
        }

        $password = md5($password);

        if (!$DB->begin_transaction()){
            returnJSONStatus(false, "A database error has occured.");
        }

        $query = "INSERT INTO pms_fts_users(email, password) VALUES('$email','$password')";
        $query_result = $DB->query($query);

        if (!$query_result){
            $errno = $DB->errno;
            $DB->rollback();
            returnJSONStatus(false, $errno === 1062 ? "An account with this email has been already registered." : "A database error has occured.");
        }

        $DEFAULT_ACTIVITIES = [
            ["Not assigned", "707070"],
            ["Cooking", "E04C5B"],
            ["School", "C3842A"],
            ["Sleep", "4D8DDA"],
            ["Shower", "40BACC"],
            ["Television", "BABB38"],
            ["Travel", "9993AE"],
            ["Work", "785539"]
        ];

        $user_id = $DB->insert_id;

        foreach ($DEFAULT_ACTIVITIES as $key => $value) {
            $query = "INSERT INTO pms_fts_activities(user_id, name, color) VALUES($user_id, '$value[0]','$value[1]')";
            $query_result = $DB->query($query);

            if (!$query_result){
                $DB->rollback();
                returnJSONStatus(false, "A database error has occured.");
            }
        }

        if (!$DB->commit()){
            $DB->rollback();
            returnJSONStatus(false, "A database error has occured.");
        }
        returnJSONStatus(true, "You have been succesfully registered.");
        break;
    case "getAllActivities" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $query = "SELECT id, name, CONCAT('#', color) color FROM pms_fts_activities WHERE user_id = '$user_id' ORDER BY name ASC;";
        $result = $DB->query($query);

        if (!$result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }
        echo json_encode($result->fetch_all(MYSQLI_ASSOC));
        break;

    case "getRecordsRange" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $from = $DB->escape_string($_POST["date_from"]);
        $to = $DB->escape_string($_POST["date_to"]);
        $query = "SELECT r.id, r.activity_id, r.date, LEFT(r.begin, 5) begin, LEFT(r.end, 5) end FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.date BETWEEN '$from' AND '$to' ORDER BY r.begin ASC, r.end ASC";
        $result = $DB->query($query);

        if (!$result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }
        echo json_encode($result->fetch_all(MYSQLI_ASSOC));
        break;

    case "pantyDropperMonster" :
        //vsetky oci na mne vsetci vidia
        $user_id = $DB->escape_string($_POST["user_id"]);
        $from = $DB->escape_string($_POST["date_from"]);
        $to = $DB->escape_string($_POST["date_to"]);
        $query = "SELECT a.name, a.color, x.begin, x.end FROM pms_fts_activities a RIGHT OUTER JOIN (SELECT r.id, r.activity_id, r.date, LEFT(r.begin, 5) begin, LEFT(r.end, 5) end FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.date BETWEEN '$from' AND '$to' ORDER BY r.begin ASC) x ON a.id = x.activity_id";
        //závidia, z princípu nevidia, nech vidia
        $result = $DB->query($query);

        if (!$result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }
        echo json_encode($result->fetch_all(MYSQLI_ASSOC));
        break;

    case "newRecord" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $date = $DB->escape_string($_POST["date"]);
        $end = $DB->escape_string($_POST["end"]);

        $query = "SELECT live_tracking_started FROM pms_fts_users WHERE id = '$user_id' AND live_tracking_started IS NOT NULL";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($date." ".$end, $row[0])){
                if (isLater($date, substr($row[0], 0, 10))) {
                    $end = "00:00";
                }
                else
                    $end = substr($row[0], 11, 5);
            }
        }

        $query_result->close();

        $query = "SELECT LEFT(r.end, 5) FROM pms_fts_records r INNER JOIN pms_fts_activities a ON r.activity_id = a.id AND a.user_id = '$user_id' WHERE r.date = '$date' ORDER BY r.end DESC LIMIT 1";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $begin = "00:00";
        if ($row = $query_result->fetch_row()) {
            $begin = $row[0];
        }
        $query_result->close();

        $query = "SELECT id FROM pms_fts_activities WHERE user_id = '$user_id' AND name = 'Not assigned'";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()) {
            $activity_id = $row[0];
        }
        else {
            http_response_code(500);
            echo "Invalid user.";
            exit();
        }
        $query_result->close();

        $query = "INSERT INTO pms_fts_records(activity_id, date, begin, end) VALUES('$activity_id', '$date', '$begin', '$end')";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $record_id = $DB->insert_id;

        echo json_encode(array("id" => $record_id, "activity_id" => $activity_id, "date" => $date, "begin" => $begin, "end" => $end));
        exit();
        break;
    case "deleteRecord" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $record_id = $DB->escape_string($_POST["record_id"]);

        $query = "DELETE r FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.id = '$record_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        break;
    case "setRecordActivity":
        $user_id = $DB->escape_string($_POST["user_id"]);
        $record_id = $DB->escape_string($_POST["id"]);
        $activity_name = strtolower(fullyEscape($_POST["activity_name"]));

        $query = "SELECT * FROM pms_fts_activities WHERE user_id = '$user_id' AND name = '$activity_name'";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $result = $query_result->fetch_assoc();
        $query_result->close();

        if ($result !== null){
            $activity_id = $result["id"];

            $query = "UPDATE pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id SET r.activity_id = '$activity_id' WHERE a.user_id = '$user_id' AND r.id = '$record_id'";
            $query_result = $DB->query($query);

            if (!$query_result){
                http_response_code(500);
                echo "A database error has occured.";
                exit();
            }
        }

        echo json_encode($result);
        exit();
        break;
    case "setRecordBeginTime" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $record_id = $DB->escape_string($_POST["id"]);
        $begin = $DB->escape_string($_POST["begin"]);
        $end = $DB->escape_string($_POST["end"]);
        $date = $DB->escape_string($_POST["date"]);
        $upper_limit = $DB->escape_string($_POST["upper_limit"]);

        if ($upper_limit !== ""){
            if (isLater($begin, $upper_limit)){
                $begin = $upper_limit;
                $end = $upper_limit;
            }
            else if (isLater($end, $upper_limit)){
                $end = $upper_limit;
            }
        }

        $query = "SELECT live_tracking_started FROM pms_fts_users WHERE id = '$user_id' AND live_tracking_started IS NOT NULL";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($date." ".$end, $row[0])){
                if (isLater($date, substr($row[0], 0, 10))) {
                    $begin = "00:00";
                    $end = "00:00";
                }
                else
                    $end = substr($row[0], 11, 5);
            }
        }

        $query_result->close();

        if (isLater($begin, $end)){
            $end = $begin;
        }

        $query = "SELECT LEFT(r.end, 5) FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.date = '$date' AND r.begin < '$begin' AND r.id != '$record_id' ORDER BY r.begin DESC, r.end DESC LIMIT 1";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($row[0], $begin)){
                $begin = $row[0];
            }
        }
        $query_result->close();

        if (isLater($begin, $end)){
            $end = $begin;
        }

        $query = "SELECT LEFT(r.begin, 5) FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.date = '$date' AND r.begin >= '$begin' AND r.id != '$record_id' ORDER BY r.begin ASC LIMIT 1";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($end, $row[0])){
                $end = $row[0];
            }
        }
        $query_result->close();

        $query = "UPDATE pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id SET r.begin = '$begin', r.end = '$end' WHERE a.user_id = '$user_id' AND r.id = '$record_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        echo json_encode(array("begin" => $begin, "end" => $end));

        break;
    case "setRecordEndTime" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $record_id = $DB->escape_string($_POST["id"]);
        $begin = $DB->escape_string($_POST["begin"]);
        $end = $DB->escape_string($_POST["end"]);
        $date = $DB->escape_string($_POST["date"]);
        $upper_limit = $DB->escape_string($_POST["upper_limit"]);

        if ($upper_limit !== ""){
            if (isLater($begin, $upper_limit)){
                $begin = $upper_limit;
                $end = $upper_limit;
            }
            else if (isLater($end, $upper_limit)){
                $end = $upper_limit;
            }
        }

        $query = "SELECT live_tracking_started FROM pms_fts_users WHERE id = '$user_id' AND live_tracking_started IS NOT NULL";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($date." ".$end, $row[0])){
                if (isLater($date, substr($row[0], 0, 10)))
                    $end = "00:00";
                else
                    $end = substr($row[0], 11, 5);
            }
        }

        $query_result->close();

        if (isLater($begin, $end)){
            $begin = $end;
        }

        $query = "SELECT LEFT(r.begin, 5) FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.date = '$date' AND r.end > '$end' AND r.id != '$record_id' ORDER BY r.begin ASC LIMIT 1";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($end, $row[0])){
                $end = $row[0];
            }
        }
        $query_result->close();

        if (isLater($begin, $end)){
            $begin = $end;
        }

        $query = "SELECT LEFT(r.end, 5) FROM pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id WHERE a.user_id = '$user_id' AND r.date = '$date' AND r.end <= '$end' AND r.id != '$record_id' ORDER BY r.end DESC LIMIT 1";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        if ($row = $query_result->fetch_row()){
            if (isLater($row[0], $begin)){
                $begin = $row[0];
            }
        }

        $query_result->close();

        $query = "UPDATE pms_fts_records r LEFT OUTER JOIN pms_fts_activities a ON r.activity_id = a.id SET r.begin = '$begin', r.end = '$end' WHERE a.user_id = '$user_id' AND r.id = '$record_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        echo json_encode(array("begin" => $begin, "end" => $end));

        break;
    case "fetchLiveTracking" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $query = "SELECT a.name, u.live_tracking_started begin FROM pms_fts_users u LEFT OUTER JOIN pms_fts_activities a ON u.live_tracking_activity_id = a.id WHERE u.id = '$user_id'";
        $query_result = $DB->query($query);

        if (!$query_result || $query_result->num_rows === 0){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        echo json_encode($query_result->fetch_assoc());
        break;
    case "startLiveTracking" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $datetime = $DB->escape_string($_POST["datetime"]);

        $query = "SELECT id FROM pms_fts_activities WHERE user_id = '$user_id' AND name = 'Not assigned'";
        $query_result = $DB->query($query);

        if (!$query_result || $query_result->num_rows === 0){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $default_id = $query_result->fetch_row()[0];

        $query = "UPDATE pms_fts_users SET live_tracking_started = '$datetime', live_tracking_activity_id = '$default_id' WHERE id = '$user_id' AND live_tracking_started IS NULL";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        break;
    case "setLiveTrackingActivity" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $activity_name = $DB->escape_string($_POST["activity_name"]);

        $query = "SELECT id FROM pms_fts_activities WHERE user_id = '$user_id' AND name = '$activity_name'";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $result = $query_result->fetch_row();
        $query_result->close();

        if ($result !== null){
            $activity_id = $result[0];

            $query = "UPDATE pms_fts_users SET live_tracking_activity_id = '$activity_id' WHERE id = '$user_id'";
            $query_result = $DB->query($query);

            if (!$query_result){
                http_response_code(500);
                echo "A database error has occured.";
                exit();
            }

            returnJSONStatus(true, "");
        }

        returnJSONStatus(false, "");
        break;
    case "stopLiveTracking" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $datetime = $DB->escape_string($_POST["datetime"]);

        $stopDT = date_create_from_format("Y-m-d H:i", $datetime);

        if (!$stopDT) {
            if (!$query_result){
                http_response_code(500);
                echo "Invalid datetime format.";
                exit();
            }
        }

        $query = "SELECT live_tracking_activity_id, live_tracking_started FROM  pms_fts_users WHERE id = '$user_id' AND live_tracking_activity_id IS NOT NULL AND live_tracking_started IS NOT NULL";
        $query_result = $DB->query($query);

        if (!$query_result){
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $row = $query_result->fetch_row();

        if (!$row){
            http_response_code(500);
            echo "Tracking not started for this user.";
            exit();
        }

        $activity_id = $row[0];
        $startDT =  date_create_from_format("Y-m-d H:i:s", $row[1]);

        $query_result->close();

        $timeFrom = $startDT->format("H:i");
        $timeTo = $stopDT->format("H:i");

        $startDT->setTime(0, 0, 0);
        $stopDT->setTime(0, 0, 0);

        if (!$DB->begin_transaction()) {
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        while ($startDT < $stopDT){
            $date = $startDT->format("Y-m-d");
            $query = "INSERT INTO pms_fts_records(activity_id, date, begin, end) VALUES('$activity_id', '$date', '$timeFrom', '23:59')";
            $query_result = $DB->query($query);

            if (!$query_result){
                $DB->rollback();
                http_response_code(500);
                echo "A database error has occured.";
            }

            $startDT->modify("+1 day");
            $timeFrom = "00:00";
        }

        if ($startDT == $stopDT){
            $date = $startDT->format("Y-m-d");
            $query = "INSERT INTO pms_fts_records(activity_id, date, begin, end) VALUES('$activity_id', '$date', '$timeFrom', '$timeTo')";
            $query_result = $DB->query($query);

            if (!$query_result){
                $DB->rollback();
                http_response_code(500);
                echo "A database error has occured.";
            }
        }

        $query = "UPDATE pms_fts_users SET live_tracking_started = NULL, live_tracking_activity_id = NULL WHERE id = '$user_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            $DB->rollback();
            http_response_code(500);
            echo "A database error has occured.";
            exit();
        }

        $DB->commit();
        exit();
        break;
    case "newActivity" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $name = fullyEscape($_POST["name"]);
        $color = sprintf('%06X', mt_rand(0, 0xFFFFFF));

        if (strlen($name) === 0){
            returnJSONStatus(false, "The activity must have a non-empty name.");
        }

        $query = "INSERT INTO pms_fts_activities(user_id, name, color) VALUES('$user_id','$name', '$color')";
        $query_result = $DB->query($query);

        if (!$query_result){
            $errno = $DB->errno;
            returnJSONStatus(false, $errno === 1062 ?"You already have an activity named ".trim($_POST["name"])."." : "A database error has occured.");
        }

        $activity_id = $DB->insert_id;

        echo json_encode(array(
            "success" => true,
            "activity" => array(
                "id" => $activity_id,
                "name" => $name,
                "color" => "#".$color
            )
        ));
        exit();
        break;
    case "deleteActivity" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $activity_id = $DB->escape_string($_POST["user_id"]);

        $query = "DELETE FROM pms_fts_activities WHERE user_id = '$user_id' AND id = '$activity_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            $errno = $DB->errno;
            returnJSONStatus(false, "A database error has occured.");
        }

        returnJSONStatus(true, "");
        break;
    case "setActivityName" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $activity_id = $DB->escape_string($_POST["activity_id"]);
        $name = fullyEscape($_POST["name"]);

        if (strlen($name) === 0){
            returnJSONStatus(false, "The activity must have a non-empty name.");
        }

        $query = "UPDATE pms_fts_activities SET name = '$name' WHERE user_id = '$user_id' AND id = '$activity_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            $errno = $DB->errno;
            returnJSONStatus(false, $errno === 1062 ?"You already have an activity named ".trim($_POST["name"])."." : "A database error has occured.");
        }

        returnJSONStatus(true, "");
        exit();
        break;
    case "setActivityColor" :
        $user_id = $DB->escape_string($_POST["user_id"]);
        $activity_id = $DB->escape_string($_POST["activity_id"]);
        $color = fullyEscape($_POST["color"]);

        $query = "UPDATE pms_fts_activities SET color = '$color' WHERE user_id = '$user_id' AND id = '$activity_id'";
        $query_result = $DB->query($query);

        if (!$query_result){
            $errno = $DB->errno;
            returnJSONStatus(false, "A database error has occured.");
        }
        returnJSONStatus(true, "");
        exit();
        break;
    default:
        break;
}