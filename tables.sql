--
-- Databáza: `pms_fts`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `pms_fts_activities`
--

CREATE TABLE `pms_fts_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `color` char(6) CHARACTER SET ascii NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `pms_fts_records`
--

CREATE TABLE `pms_fts_records` (
  `id` int(10) UNSIGNED NOT NULL,
  `activity_id` int(10) UNSIGNED DEFAULT NULL,
  `date` date NOT NULL,
  `begin` time NOT NULL,
  `end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `pms_fts_users`
--

CREATE TABLE `pms_fts_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(32) CHARACTER SET ascii NOT NULL,
  `live_tracking_started` datetime DEFAULT NULL,
  `live_tracking_activity_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `pms_fts_activities`
--
ALTER TABLE `pms_fts_activities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`name`),
  ADD KEY `pms_fts_activities_ibfk_1` (`user_id`);

--
-- Indexy pre tabuľku `pms_fts_records`
--
ALTER TABLE `pms_fts_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pms_fts_records_ibfk_1` (`activity_id`),
  ADD KEY `date` (`date`),
  ADD KEY `begin` (`date`,`begin`);

--
-- Indexy pre tabuľku `pms_fts_users`
--
ALTER TABLE `pms_fts_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `pms_fts_users_ibfk_1` (`live_tracking_activity_id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `pms_fts_activities`
--
ALTER TABLE `pms_fts_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `pms_fts_records`
--
ALTER TABLE `pms_fts_records`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `pms_fts_users`
--
ALTER TABLE `pms_fts_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `pms_fts_activities`
--
ALTER TABLE `pms_fts_activities`
  ADD CONSTRAINT `pms_fts_activities_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `pms_fts_users` (`id`) ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuľku `pms_fts_records`
--
ALTER TABLE `pms_fts_records`
  ADD CONSTRAINT `pms_fts_records_ibfk_1` FOREIGN KEY (`activity_id`) REFERENCES `pms_fts_activities` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuľku `pms_fts_users`
--
ALTER TABLE `pms_fts_users`
  ADD CONSTRAINT `pms_fts_users_ibfk_1` FOREIGN KEY (`live_tracking_activity_id`) REFERENCES `pms_fts_activities` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
