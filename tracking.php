<?php

require_once "common.php";

head($C_TITLES[0]);

?>

    <section id="live-tracking">
        <h2>
            Live tracking:
        </h2>
        <div>
            <button class="live-start"><i class="fa fa-play" aria-hidden="true"></i> Start</button>
            <table class="live-details">
                <tr>
                    <td><strong>Started:</strong> <span class="live-begin"></span></td>
                    <td class="live-time" rowspan="2">0s</td>
                </tr>
                <tr>
                    <td><strong>Activity:</strong> <input class="awesomplete live-activity"/></td>
                </tr>
            </table>
        </div>
    </section>
    <section id="record-list">
        <header class="clear-after">
            <h2 class="date-picker-trigger"><i class="fa fa-calendar" aria-hidden="true"></i> <span id="date-range"></span></h2>
        </header>

        <div id="record-table">
            <div>
                <h4>Activity</h4><h4>Begin</h4><h4>End</h4>
            </div>
        </div>

        <div style="text-align: center;">
            <button class="record-add btn-green"><i>+</i> Add record</button>
        </div>

    </section>

<?php

footer();