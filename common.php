<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
require_once "database.php";

define("TITLES", serialize(array("FTS | Tracking", "FTS | Reports", "FTS | Activities")));

$C_TITLES = unserialize(TITLES);
$C_ALIASES = [$C_TITLES[0] => "tracking",
    $C_TITLES[1] => "reports",
    $C_TITLES[2] => "activities"];

function head($title){
    global $C_TITLES, $C_ALIASES;

    if (!is_user_logged_in()){
        header("Location: /");
    }

    setcookie("user", json_encode($_SESSION['user']), $_SESSION['user']['remember'] ? time() + 60 * 60 * 24 * 30 : 0, "/");
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title><?= $title ?></title>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="bower_components/jquery-ui/themes/pepper-grinder/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="bower_components/awesomplete/awesomplete.css">
        <link rel="stylesheet" type="text/css" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="bower_components/spectrum/spectrum.css">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link rel="stylesheet" type="text/css" media="(max-width: 960px)" href="css/mobile.css">

        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
        <script src="bower_components/jquery.cookie/jquery.cookie.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/moment/min/moment.min.js"></script>
        <script src="bower_components/tinycolor/dist/tinycolor-min.js"></script>
        <script src="bower_components/spectrum/spectrum.js"></script>
        <script src="bower_components/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
        <script src="bower_components/awesomplete/awesomplete.min.js"></script>
        <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="js/main.js"></script>
        <script src="js/<?= $C_ALIASES[$title] ?>.js"></script>
        <script src="js/after.js"></script>
    </head>
    <body>
    <header class="clear-after" >
        <nav>
            <a href="tracking.php"><h1>FTS</h1></a>
            <a class="<?= $title === $C_TITLES[0] ? "active" : "" ?>" href="tracking.php">Tracking</a>
            <a class="<?= $title === $C_TITLES[1] ? "active" : "" ?>" href="reports.php">Reports</a>
            <a class="<?= $title === $C_TITLES[2] ? "active" : "" ?>" href="activities.php">Activities</a>
        </nav>

        <menu>
            logged in as <strong><?= $_SESSION['user']['email'];?></strong>
            <button id="logout" class="btn-red" >
                <i class="fa fa-power-off" aria-hidden="true"></i> Log out
            </button>
        </menu>
    </header>
    <main id="<?= $C_ALIASES[$title] ?>">
    <?php
};

function footer() { ?>
    </main>
    <footer>
        For Track's Sake © Peter Paulovics, Stanislav Krajčovič. 2016. All rights reserved.
    </footer>
    <div class="helper-popups">
        <div id="page-dim"></div>
        <img id="ajax-anim" src="images/ajax-loader.gif" alt="ajax">
    </div>
    </body>
</html>
    <?php
}